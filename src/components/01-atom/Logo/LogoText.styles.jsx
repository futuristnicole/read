import styled, { css } from 'styled-components';
// import colors from '../../00-base/colors.styles';
import { BrandRed, BrandBlack } from '../../00-base/colors.styles';
import { FontFamilyLogo } from '../../00-base/font.styles';


export const LogoText = css`
    font-family: ${FontFamilyLogo};
`;
export const LogoRead = styled.span`
    ${LogoText}
    color: ${BrandRed};
`;

export const LogoBoot = styled.span`
    ${LogoText}
    color: ${BrandBlack};
`;