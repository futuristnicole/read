import React, { Fragment } from 'react';

import { LogoRead, LogoBoot } from './LogoText.styles';

export const LogoText = () => (
  <Fragment>
    <LogoRead>READ</LogoRead><LogoBoot>BOOT</LogoBoot>
  </Fragment>
);

